#ifndef GAME_H_
#define GAME_H_

#include "Player.h"
#include "SDL2Common.h"

typedef struct Game {
	static const int WINDOW_WIDTH = 800;
	static const int WINDOW_HEIGHT = 600;

	SDL_Window* gameWindow = nullptr;
	SDL_Renderer* gameRenderer = nullptr;

	SDL_Texture* backgroundTexture = nullptr;
	Player player;

	SDL_Event event;
	bool quit = false;

	const Uint8* keyStates;

	unsigned int currentTimeIndex;
	unsigned int prevTimeIndex;
	unsigned int timeDelta;
	float timeDeltaInSeconds;
} Game;

bool initGame(Game* game);
void runGameLoop(Game* game);
void processInputs(Game* game);
void updateGame(Game* game, float timeDelta);
void drawGame(Game* game);
void cleanUpGame(Game* game);

#endif